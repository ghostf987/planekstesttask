from django.conf.urls import url
from . import views

urlpatterns = [
    # post views
    url(r'^login/$', views.user_login, name='login'),
    url(r'^main/$', views.main, name='main'),
    url(r'^logout/$', views.logout_view, name='logout'),
]